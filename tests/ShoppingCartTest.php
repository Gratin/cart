<?php
namespace Gratin\Cart\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Gratin\Cart\Facades\CartFacade;

class ShoppingCartTest extends TestCase
{
    /**
     */
    public function testCanAccessStepsWithoutCart()
    {
    }

    /**
     * Can add items to cart
     *
     * @return void
     */
    public function testCanAddItemToCart()
    {
        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);
        $response->assertStatus(200);
    }

    /**
     * Wrong add request
     *
     * @return void
     */
    public function testAddItemWrongRequest()
    {
        $response = $this->post('/cart/add', ['product_id'=>3614]);
        $response->assertStatus(422);

        $response = $this->post('/cart/add', ['packaging_id'=>3614]);
        $response->assertStatus(422);
    }

    /**
     * Test if we can remove an item from the cart
     *
     * @return void
     */
    public function testRemoveItemFromCart()
    {
        $emptyCart = $this->get('/cart');
        $emptyCart = json_decode($emptyCart->getContent(), true);

        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);
        $response->assertStatus(200);

        $response = $this->post('/cart/remove', ['product_id'=>3614]);
        $response->assertStatus(200);
        $response->assertJson($emptyCart);
    }

    /**
     * Test if we can remove an item from the cart
     *
     * @return void
     */
    public function testAddSameProductQuantity()
    {
        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);
        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);
        $response->assertStatus(200);

        $response->assertJsonFragment(['quantity'=>2]);
    }

    /**
     * Test if we can remove an item from the cart
     *
     * @return void
     */
    public function testDeleteProductQuantity()
    {
        $emptyCart = $this->get('/cart');
        $emptyCart = json_decode($emptyCart->getContent(), true);

        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);
        $response = $this->post('/cart/delete', ['product_id'=>3614]);

        $response->assertStatus(200);
        $response->assertJson($emptyCart);
    }

    /**
     * Test if we can delete a product that is currently not in our cart
     *
     * @return void
     */
    public function testDeleteProductNotInCart()
    {
        $emptyCart = $this->get('/cart');
        $emptyCart = json_decode($emptyCart->getContent(), true);

        $response = $this->post('/cart/delete', ['product_id'=>3614]);
        $response->assertStatus(422);
        $response->assertJsonStructure(['status', 'message']);
    }

    /**
     * Tests set quantity of product
     *
     * @return void
     */
    public function testSetQuantity()
    {
        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1, 'quantity'=>5]);

        $response->assertStatus(200);
        $response->assertJsonFragment(['quantity'=>5]);

        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1, 'quantity'=>2]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['quantity'=>2]);

        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1, 'quantity'=>220]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['quantity'=>220]);

        $response = $this->post('/cart/delete', ['product_id'=>3614]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['items'=>[], 'total'=>"0.00"]);
    }

    /**
     * Tests a complex cart operation
     *
     * @return void
     */
    public function testUserProcess()
    {
        $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);

        $productHelper = new \App\Helpers\ProductHelper();
        $product = [3614=>$productHelper->retrieve('tea_3614'), 3815=>$productHelper->retrieve('tea_3815'), 3586=>$productHelper->retrieve('tea_3586')];

        $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1, 'quantity'=>5]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['id'=>$product[3614]->getReadableId(), 'quantity'=>5]);

        $response = $this->post('/cart/add', ['product_id'=>3815, 'packaging_id'=>1, 'quantity'=>53]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['id'=>$product[3815]->getReadableId(), 'quantity'=>53]);

        $response = $this->post('/cart/add', ['product_id'=>3815, 'packaging_id'=>1]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['id'=>$product[3815]->getReadableId(), 'quantity'=>54]);

        $response = $this->post('/cart/add', ['product_id'=>3586, 'packaging_id'=>1]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['id'=>$product[3586]->getReadableId(), 'quantity'=>1]);

        $response = $this->post('/cart/remove', ['product_id'=>3586, 'packaging_id'=>1]);
        $response->assertStatus(200);
        $response->assertJsonMissing(['id'=>$product[3586]->getReadableId()]);

        $response = $this->post('/cart/delete', ['product_id'=>3614]);
        $response = $this->post('/cart/delete', ['product_id'=>3815]);
        $response->assertStatus(200);
        $response->assertExactJson(['cart'=>['items'=>[], 'total'=>"0.00"]]);
    }

    // /**
    //  * Tests a complex cart operation
    //  *
    //  * @return void
    //  */
    // public function testPaymentProcess()
    // {
    //     $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1]);

    //     $productHelper = new \App\Helpers\ProductHelper();
    //     $product = [3614=>$productHelper->retrieve(3614), 3815=>$productHelper->retrieve(3815), 3586=>$productHelper->retrieve(3586)];

    //     $response = $this->post('/cart/add', ['product_id'=>3614, 'packaging_id'=>1, 'quantity'=>5]);
    //     $response->assertStatus(200);
    //     $response->assertJsonFragment(['id'=>$product[3614]->getReadableId(), 'quantity'=>5]);

    //     $response = $this->post('/cart/add', ['product_id'=>3815, 'packaging_id'=>1, 'quantity'=>53]);
    //     $response->assertStatus(200);
    //     $response->assertJsonFragment(['id'=>$product[3815]->getReadableId(), 'quantity'=>53]);

    //     $response = $this->post('/cart/add', ['product_id'=>3815, 'packaging_id'=>1]);
    //     $response->assertStatus(200);
    //     $response->assertJsonFragment(['id'=>$product[3815]->getReadableId(), 'quantity'=>54]);

    //     $response = $this->post('/cart/add', ['product_id'=>3586, 'packaging_id'=>1]);
    //     $response->assertStatus(200);
    //     $response->assertJsonFragment(['id'=>$product[3586]->getReadableId(), 'quantity'=>1]);

    //     $response = $this->post('/cart/process');
    //     $response->assertStatus(200);
    //     $response->assertExactJson(['success'=>true]);
    // }
}
