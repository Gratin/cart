<?php

namespace Gratin\Cart\Providers;

use Illuminate\Support\ServiceProvider;

class GratinCartProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/cart.php');

        $this->publishes([
            __DIR__.'/../config/cart.php' => config_path('cart.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__.'/../migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/cart.php',
            'cart'
        );
    }
}
