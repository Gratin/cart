<?php
return [
    'controller'			=>	'\\Gratin\\Cart\\Controllers\\CartController',
    'manager'				=>	'\\Gratin\\Cart\\Managers\\CartManager',
    'item_manager'		=>	'\\Gratin\\Cart\\Helpers\\ProductHelper',
    'product_model'			=>	'\\Gratin\\Cart\\Models\\Product',
    'item_transformer'	=>	'\\Gratin\\Cart\\Transformers\\ProductTransformer',
    'currency_manager'		=>	'\\Gratin\\Cart\\Managers\\CurrencyManager',
    'cart_model'            =>  '\\Gratin\\Cart\\Models\\Cart',
    'taxes'					=>	[
        'en_CA'	=>	[
            'rates'	=>	[
                0.005, 0.09975
            ],
            'currency'	=>	'CAD',
        ],
        'en_US'	=>	[
            'currency'	=>	'USD',
        ]
    ],
    'auth_guard'            =>  'clients',
    'strict'                =>  true,
    'event_system'          =>  'laravel',
    /*
    'payment_methods'       =>  [
        'bambora'           =>  '\\Gratin\\Payment\\Adapters\\BamboraAdapter@processPayment',
        'paypal'            =>  '\\Gratin\\Payment\\Adapters\\PaypalAdapter@processPayment',
    ],
    */
    // 'shipping_manager'       =>  '\\Gratin\\Cart\\Managers\\ShippingManager',
    // 'payment_manager'        =>  '\\Gratin\\Payment\\Adapters\\BamboraAdapter@processPayment',
    // 'payment_bridge'         =>  '\\Gratin\\Cart\\Bridges\\BamboraBridge',
];
