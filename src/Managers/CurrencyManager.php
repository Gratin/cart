<?php
namespace Gratin\Cart\Managers;

use \NumberFormatter;

class CurrencyManager
{
    /**
     * @Todo: Refactor exception handling
     * Computes taxes for a given amount from the configuration relative to the country argument
     * @param float $total The amount to tax
     * @param ?string $country The country code found in the configuration
     * @return array ['tax$i[name, value, rate]', total]
     */
    public function tax($taxable, $realTotal, ?string $country = 'en_CA'): array
    {
        $config = config('cart.taxes');

        if (empty($config)) {
            throw new \Exception('no_tax_rates');
        }

        if (!array_key_exists($country, $config)) {
            throw new \Exception('country_taxes_not_found');
        }

        $rates = $config[$country]['rates'];
        $base = $realTotal;
        $taxes = [];
        $index = 1;

        foreach ($rates as $name => $rate) {
            $tx['name'] = $name;

            $tx['rate'] = $rate;

            if ($taxable !== 0) {
                $tx['value'] = $rate;
                $realTotal += (float)$tx['value'];
            } else {
                $tx['value'] = 0;
                $realTotal += 0;
            }
            $taxes['tax_'.$index] = $tx;
            $index++;
        }

        $taxes['total'] = $realTotal;

        return $taxes;
    }
}
