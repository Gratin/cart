<?php
namespace Gratin\Cart\Managers;

use Gratin\Cart\Models\Item;
use Gratin\Cart\Events\CartActionEvent;
use Gratin\Cart\Interfaces\CartInterface;

class CartManager
{
    const EXPIRATION_DELAY = 20;

    private $guard;
    private $isGuarded = false;

    private $currencyManager;

    public function __construct(array $configs)
    {
        if (isset($configs['auth_guard'])) {
            $this->isGuarded = true;
            $this->guard = $configs['auth_guard'];
        }
     
        if (isset($configs['currency_manager'])) {
            $this->currencyManager = new $configs['currency_manager']();
        }
    }

    /**
     * Gets the cart instance from either the session OR the database
     * @param string $class The Cart Model class name
     * @param bool $refresh Wether we should refresh (to un-expire) the cart or not
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function getOrCreate(string $class, bool $refresh = false): CartInterface
    {
        $cart   = $user = null;
        $sessId = \Session::get('cart_session_id');

        // If there is guard configured, we try to find the user
        // And the cart associated
        if ($this->isGuarded) {
            try {
                $user = \Auth::guard($this->guard)->user();
            } catch (\Exception $e) {
            }
            if ($user) {
                if ($cart = $this->findCartBy($class, 'user_id', $user->id)) {
                    if ($cart->session_id !== $sessId) {
                        \DB::beginTransaction();
                        try {
                            $cart->session_id = null;

                            if ($refresh) {
                                $cart->expires_at   = new \DateTime("+".self::EXPIRATION_DELAY." minutes");
                                $cart->expired      = false;
                                event(new CartActionEvent($cart, 'refresh'));
                            } else {
                                $this->isExpired($cart);
                            }

                            $cart->save();
                            \DB::commit();
                        } catch (\Exception $e) {
                            \DB::rollback();
                            throw $e;
                            throw new \Exception('cant_update_cart');
                        }
                    }
                    return $cart;
                }
            }
        }

        // If no cart were found in the database, we then find it by
        // Session id
        if (!$cart) {
            try {
                $cart = $this->findCartBy($class, 'session_id', $sessId, $refresh);
            } catch (\Exception $e) {
                \DB::rollback();
                throw new \Exception('cant_update_cart');
            }
        }

        // Else we create a new cart instance
        if (!$cart) {
            \DB::beginTransaction();
            try {
                $cart = new $class();
                $cart->session_id = $sessId;
                $cart->expires_at = new \DateTime("+".self::EXPIRATION_DELAY." minutes");

                if ($user) {
                    $cart->user_id = $user->id;
                }

                $cart->save();
                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollback();
                throw new \Exception('cant_create_cart');
            }
        }

        return $cart;
    }

    /**
     * Query to find the cart by a given field and value
     * We look through all the cart associated with a given user
     * @param string $class The class name of the cart Model
     * @param string $field The field to query
     * @param mixed $value The value to find
     * @param bool $refresh If we should refresh the cart or not
     * @return null|\Gratin\Cart\Interfaces\CartInterface
     */
    private function findCartBy(string $class, string $field, $value, bool $refresh = false): ?CartInterface
    {
        $carts = $class::where([
            $field => $value,
            'open' => true
        ])->orderBy('expires_at', 'DESC')->limit(2)->get()->all();

        $cart  = false;
        $count = count($carts);
        $startCount = $count;

        if ($count <= 0) {
            return null;
        }
    
        if ($carts[0]->expired === 0) {
            $cart = $carts[0];
        } elseif (isset($carts[1])) {
            $cart = $cart[1];
        } else {
            $cart = $carts[0];
        }

        if ($refresh) {
            $cart->expired      = false;
            $cart->expires_at   = new \DateTime("+20 minutes");
        } else {
            if (!is_null($cart)) {
                $this->isExpired($cart);
            }
        }

        \DB::beginTransaction();
        try {
            if (!is_null($cart)) {
                $cart->save();
                \DB::commit();
            }
        } catch (\Exception $e) {
            throw $e;
        }

        return $cart;
    }

    public function findByOrderId($model, $orderId)
    {
        return $model::where('extras', 'like', '%odoo_order_id\":'.$orderId.'%')->first();
    }

    /**
     * Adds an item to cart or increments its quantity
     * @param \Gratin\Cart\Models\Item $item The item to add
     * @param \Gratin\Cart\Interfaces\CartInterface $cart The cart
     * @param null|int $quantity The quantity to set the product to
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function add(Item $item, CartInterface $cart, ?int $quantity = null): CartInterface
    {
        \DB::beginTransaction();

        try {
            $cart = $cart->add($item, $quantity);
            $cart->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }

        event(new CartActionEvent($cart, 'add', []));
        // Refresh the collection
        return $cart->fresh();
    }

    /**
     * Decrements the quantity of an item
     * @param \Gratin\Cart\Models\Item $item The item to add
     * @param \Gratin\Cart\Interfaces\CartInterface $cart The cart
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function remove(Item $item, CartInterface $cart): CartInterface
    {
        \DB::beginTransaction();
        try {
            $cart = $cart->sub($item);
            $cart->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw new \Exception('cant_remove_from_cart');
        }

        event(new CartActionEvent($cart, 'remove', []));
        // Refresh the collection
        return $cart->fresh();
    }

    /**
     * Deletes an item from the cart
     * @param \Gratin\Cart\Models\Item $item The item to add
     * @param \Gratin\Cart\Interfaces\CartInterface $cart The cart
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function delete(Item $item, CartInterface $cart): CartInterface
    {
        \DB::beginTransaction();
        try {
            $cart = $cart->remove($item);
            $cart->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw new \Exception('cant_delete_from_cart');
        }

        event(new CartActionEvent($cart, 'add', []));
        // Refresh the collection
        return $cart->fresh();
    }

    /**
     * Returns the costs informations of a given cart from the configuration and sum the total cost (taxes and cost)
     * @param \Gratin\Cart\Interfaces\CartInterface $cart The cart
     * @return array[taxed, totalWithTaxes]
     */
    public function costInfos(CartInterface $cart): array
    {
        $total        = 0;
        $taxable      = 0;
        $untaxable    = 0;

        foreach ($cart->items()->get() as $item) {
            $itemPrice = ($item->quantity * (float)$item->unit_price);
            
            if ($item->taxable) {
                $taxable += $itemPrice;
            } else {
                $untaxable += $itemPrice;
            }

            $total += $itemPrice;
        }

        $taxed = $this->currencyManager->tax($taxable, $total);
        $fullTotal = $taxed['total'];

        return [$taxed, $total, $fullTotal];
    }


    public function shippingInfos(CartInterface $cart, $total = null)
    {
        if (!$total) {
            // get cart
        }
        $total = \App\Helpers\CurrencyManager::convertPrice($total);
        $threshold = (float) config('cart.threshold')[currency()];

        $remaining = $threshold - $total;
        if ($remaining < 0) {
            $remaining = 0;
        }

        return [0, $remaining];
    }

    /**
     * Determines wether the cart is expired and changes the cart's state
     * @param \Gratin\Cart\Interfaces\CartInterface $cart The cart
     * @return void
     */
    private function isExpired(CartInterface &$cart): void
    {
        if ($this->getDiffInMinutes($cart->expires_at, true) > static::EXPIRATION_DELAY) {
            $cart->expired = true;
        }
    }

    /**
     * Returns the last cart sold
     * @param string $class The Model to look for
     * @return null|\Gratin\Cart\Interfaces\CartInterface
     */
    public function last(string $class): ?CartInterface
    {
        $key = "session_id";
        $value = \Session::getId();

        if ($this->isGuarded) {
            if ($user = \Auth::guard($this->guard)->user()) {
                $key = "user_id";
                $value = $user->id;
            }
        }

        try {
            $last = $class::where($key, $value)
                    ->where('open', false)
                    ->has('items')
                    ->orderBy('expires_at', 'DESC')
                    ->first();
        } catch (\Exception $e) {
            // throw new \Exception('')
            return null;
        }
        
        return $last;
    }

    /**
     * Returns the difference in minutes between a date and now
     */
    private function getDiffInMinutes($dateToTest, $since = false)
    {
        $date = new \DateTime();
        if ($since) {
            $diff = $date->diff($dateToTest);
        } else {
            $diff = $dateToTest->diff($date);
        }

        $minutes = $diff->days * 24 * 60;
        $minutes += $diff->h * 60;
        $minutes += $diff->i;

        return $minutes;
    }
}
