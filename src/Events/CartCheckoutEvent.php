<?php
namespace Gratin\Cart\Events;

use Gratin\Cart\Interfaces\CartInterface;

class CartCheckoutEvent
{
    public $cart;
    public $paymentMethod;
    public $paymentResults;
    public $skipPayment;

    /**
     * An event launched when the cart is being checked out
     * @param \Gratin\Cart\Interface\CartInterface $cart The cart beingh checked out
     * @param null|string $paymentMethod The payment method used
     * @param null|array $paymentResults The payment method results
     */
    public function __construct(CartInterface $cart, string $paymentMethod = null, ?array $paymentResults = null, ?bool $skipPayment = false)
    {
        $this->cart             = $cart;
        $this->paymentMethod    = $paymentMethod;
        $this->paymentResults   = $paymentResults;
        $this->skipPayment      = $skipPayment;
    }
}
