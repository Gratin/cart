<?php
namespace Gratin\Cart\Events;

use Gratin\Cart\Interfaces\CartEventInterface;
use Gratin\Cart\Interfaces\CartInterface;

class CartActionEvent implements CartEventInterface
{
    private $cart;

    private $action;

    private $extras;

    public function __construct(CartInterface $cart, ?string $action = null, ?array $extras = null)
    {
        $this->cart     = $cart;
        $this->action   = $action;
        $this->extras   = $extras;
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getExtras()
    {
        return $this->extras;
    }
}
