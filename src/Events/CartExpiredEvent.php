<?php
namespace Gratin\Cart\Events;

use Gratin\Cart\Interfaces\CartInterface;
use Gratin\Cart\Interfaces\CartEventInterface;

class CartExpiredEvent implements CartEventInterface
{
    public $cart;
    
    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }
}
