<?php
namespace Gratin\Cart\Interfaces;

interface CartInterface
{
    public function checkUser(): bool;
}
