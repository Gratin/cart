<?php
namespace Gratin\Cart\Services;

use App\Helpers\CurrencyManager;
use Gratin\Cat\Models\Cart;
use Gratin\Cart\Models\Item;
use Illuminate\Http\Request;
use Gratin\Cart\Helpers\ConfigLoader;
use Gratin\Cart\Managers\CartManager;
use Gratin\Cart\Events\CartActionEvent;
use Gratin\Cart\Events\CartExpiredEvent;
use Gratin\Cart\Interfaces\CartInterface;
use Gratin\Cart\Events\CartCheckoutEvent;

class CartService
{
    /**
     * @var string ADD The add action string
     */
    const ADD       = "add";

    /**
     * @var string REMOVE The remove action string
     */
    const REMOVE    = "remove";

    /**
     * @var string DELETE The delete action string
     */
    const DELETE    = "delete";

    /**
    * @var string DEFAULT_PAYMENT The default payment method
    */
    public const DEFAULT_PAYMENT  = "bambora";

    /**
     * @var array $_configs The configuration array
     */
    private $_configs;

    /**
     * @var CartManager $manager The cart manager which will provide the cart
     */
    private $manager;

    /**
     * @var ItemManager $itemManager The item manager which will provide Items to the cart
     */
    private $itemManager;

    /**
     * @var \Gratin\Payment\Interfaces\BaseManagerInterface $paymentManager The payment manager that will handle the payments
     */
    private $paymentManager;

    /**
     * @var $currencyManager The class that will handle currencies and taxes
     */
    private $currencyManager;

    /**
     * @var $itemTransformer The class that will handle making item more front-friendly
     */
    private $itemTransformer;

    /**
     * @var string $paymentManagerMethod The method to be called when processing the payment of the cart
     */
    private $paymentManagerMethod;

    /**
     * @var string The Cart Model class name as a string
     */
    private $cartModelString;

    /**
    * @var mixed $bridge An instance of the bridge from the products/cart to the payment service
    */
    private $bridge;

    private $cart = null;

    public function __construct()
    {
        $this->_configs = $this->config();
        $this->manager  = new CartManager($this->_configs);
        $this->init();
    }

    /**
     * Initiliazes variables from configurations
     * @return void
     */
    protected function init(): void
    {
        $this->cartModelString      = $this->_configs['cart_model'] ?? Cart::class;
        $itemManagerClass           = $this->_configs['item_manager'] ?? null;
        $currencyManagerClass       = $this->_configs['currency_manager'] ?? null;
        $paymentMethods             = $this->_configs['payment_methods'] ?? null;
        $bridgeString               = $this->_configs['payment_bridge'] ?? null;

        if ($this->_configs['strict'] && $this->_configs['strict'] === true) {
            if (!isset($itemManagerClass, $currencyManagerClass, $paymentMethods, $this->cartModelString, $bridgeString)) {
                throw new \Exception('cart.strict_mode_enabled');
            }
        }

        try {
            $this->itemManager          = new $itemManagerClass() ?? null;
            $this->currencyManager      = new $currencyManagerClass($this->_configs['taxes']) ?? null;
            
            if (!isset($paymentMethods[static::DEFAULT_PAYMENT])) {
                throw new \Exception('default_payment_method_not_set');
            }

            list($this->paymentManagerClass, $this->paymentManagerMethod) = $this->paymentManagerFromString($paymentMethods[static::DEFAULT_PAYMENT]);
            
            $this->bridge = new $bridgeString();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Overridable configuration function if more configuration needs to be done
     * @return array
     */
    protected function config(): array
    {
        return ConfigLoader::load('cart');
    }

    /**
     * Opens an expired cart or refresh the already opened cart
     * @param bool $refresh If we want to refresh the cart
     * @return \Gratin\Cart\Interface\CartInterface
     */
    public function open(bool $refresh = false): CartInterface
    {
        $this->cart = $this->manager->getOrCreate($this->cartModelString, $refresh);

        return $this->cart;
    }

    /**
     * Function to be overriden to parse the request in a format that the service
     * can use
     * @param \Illuminuate\Http\Request
     * @return mixed
     */
    protected function handleRequest(Request $request)
    {
        return $request;
    }

    /**
     * Add an item to the cart
     * @param \Gratin\Cart\Models\Item $item The item to add
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function add(Item $item, ?int $quantity = null): CartInterface
    {
        try {
            $this->cart = $this->open(true);
            $this->cart = $this->manager->add($item, $this->cart, $quantity);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->cart->setLastItem($item);
        // $this->trigger(CartActionEvent::class, self::ADD, [$item, $quantity]);

        return $this->cart;
    }

    /**
     * Deletes an item from the cart
     * @param \Gratin\Cart\Models\Item $item The item to delete
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function delete(Item $item): CartInterface
    {
        try {
            $this->cart = $this->open(true);
            $this->cart = $this->manager->delete($item, $this->cart);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->trigger(CartActionEvent::class, self::DELETE, [$item, $item->quantity]);
        $this->cart['total_items'] = $this->getCount();

        return $this->cart;
    }

    /**
     * Removes (substracts) an item from the cart
     * @param \Gratin\Cart\Models\Item $item The item to remove
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function remove(Item $item): CartInterface
    {
        try {
            $this->cart = $this->open(true);
            $this->cart = $this->manager->remove($item, $this->cart);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->trigger(CartActionEvent::class, self::REMOVE, [$item]);

        return $this->cart;
    }

    /**
     * Processes the payment for a given user using the cart stored in session
     * @param mixed $user An instance of the user's customerId OR guest information
     * @param null|string The payment method to use, defaults to Bambora
     * @return bool
     */
    public function pay($userData, ?string $paymentMethod = null): bool
    {
        $data = $this->buildCart();
        
        try {
            if (!$paymentMethod) {
                $content = $this->paymentManager->{$this->paymentManagerMethod}($data, $userData);
            } else {
                list($manager, $method) = $this->paymentManagerFromString($this->_configs['payment_methods'][$paymentMethod] ?? null);
                $content = $manager->{$method}($data, $userData);
            }
        } catch (\Exception $e) {
            // throw $e;
            throw CartErrorHandler::paymentFailed();
        }

        if ($user = \Auth::guard($this->_configs['auth_guard'])->user()) {
            $this->cart->user = $user;
        } else {
            $this->cart->user = null;
        }
        
        event(new CartCheckoutEvent($this->cart, $paymentMethod, $content));

        return true;
    }

    /**
     * Closes the cart and set it as expired to prevent from using this cart in the future
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    public function forget(): CartInterface
    {
        $cart = $this->open();

        $cart->open = false;
        $cart->expired = true;

        try {
            $cart->save();
        } catch (\Exception $e) {
            throw new \Exception('cart.forget.error');
        }

        $this->cart = null;
        
        return $this->open(true);
    }

    /**
     *
     *
     */
    public function updateShipping($id, $options): bool
    {
        $cart = $this->open();
        $extras = $cart->extras;
        $extras['shipping'] = $options[$id];
        $cart->extras = $extras;
        try {
            $cart->save();
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    private function update()
    {
    }

    /**
     * Returns the count of items in the cart
     * @param bool $unique Wether we want the unique item count or full count
     * @return int
     */
    public function getCount(bool $unique = false): int
    {
        $cart = \Cart::open();
        if ($unique) {
            $items = $cart->items()->pluck('product_id')->all();
            return count(array_unique($items));
        }

        $count = 0;
        $itemsQty = $cart->items()->pluck('quantity')->all();
        $count = array_sum($itemsQty);

        return $count;
    }

    public function getByOrderId($orderId)
    {
        return $this->manager->findByOrderId($this->cartModelString, $orderId);
    }

    /**
     * Parses the cart in a front-friendly format
     * @return array
     */
    public function serialize(): array
    {
        if (!$this->itemTransformer) {
            $ptClass = $this->_configs['item_transformer'] ?? null;
            if ($ptClass) {
                try {
                    $this->itemTransformer = new $ptClass();
                } catch (\Exception $e) {
                    throw new \Exception('cant_create_transformer_class'. $this->_configs['item_transformer']);
                }
            } else {
                // Default transformer
            }
        }

        $cart = $this->open(true);
        $is = [];

        foreach ($cart->items()->get() as $item) {
            $prItem = $this->itemManager->retrieveById($item->product_id);
            $prItem->selectedPackage = $item->extras['packaging'] ?? null;
            $pItem = $this->itemTransformer->cartTransform($prItem);

            // TO DO Refactor this elsewhere
            $pItem['quantity']      = $item->quantity;
            if (!isset($pItem['unit_price'])) {
                $item['unit_price_card'] = $item['price'];
                $item['unit_price']      = $item['price'];
            }
            $pItem['total_price_cad']   = ($item['unit_price'] ?? $item['price']) * $item->quantity;
            $pItem['total_price']   = CurrencyManager::formatPrice(CurrencyManager::convertPrice($pItem['total_price_cad']));

            $pItem['unit_price_cad'] = $pItem['unit_price'];
            $pItem['unit_price'] = CurrencyManager::formatPrice(CurrencyManager::convertPrice($pItem['unit_price']));
            $is[] = $pItem;
        }

        list($taxes, $untaxed, $total)      = $this->manager->costInfos($cart);

        list($shipping, $isFreeShippping)   = $this->manager->shippingInfos($cart, $taxes['total']);

        if (!empty($cart->items())) {
            $lItem = $cart->items()->orderBy('updated_at', 'DESC')->first();
            if ($lItem) {
                $lprItem = $this->itemManager->retrieveById($lItem->product_id);
                $lprItem->selectedPackage = $lItem->extras['packaging'] ?? null;
                $lItem = $this->itemTransformer->cartTransform($lprItem);
            }
        }

        if (!$isFreeShippping) {
            $shipping = $c->extras['shipping'] ?? 0;
            if ($shipping === 0) {
                $shipPrice = (float)$shipping['price'];
                $total = $total + $shipPrice;
                $taxes['total'] = $total;
                $shipping = $shipping['price'];
            }
        }

        // add last product quantity
        $lItem['quantity_added'] = intval(request()->quantity ?? 1);

        /**
         * Convert and format Taxes
         */
        if (is_array($taxes)) {
            $taxes['total_taxes_cad'] = $taxes['tax_1']['value']+$taxes['tax_2']['value'];
            $taxes['total_taxes'] =  CurrencyManager::formatPrice(CurrencyManager::convertPrice($taxes['total_taxes_cad']));
        }

        if (isset($taxes['tax_1']['value'])) {
            $taxes['tax_1']['value_cad'] = $taxes['tax_1']['value'];
            $taxes['tax_1']['value'] = CurrencyManager::formatPrice(CurrencyManager::convertPrice($taxes['tax_1']['value']));
        }
        if (isset($taxes['tax_2']['value'])) {
            $taxes['tax_2']['value_cad'] = $taxes['tax_2']['value'];
            $taxes['tax_2']['value'] = CurrencyManager::formatPrice(CurrencyManager::convertPrice($taxes['tax_2']['value']));
        }

        $taxes['total_cad'] = $taxes['total'];
        $taxes['total'] = CurrencyManager::formatPrice(CurrencyManager::convertPrice($taxes['total']));

        $cart = $cart->toArray();

        $cart['sub_total_cad']          = $untaxed;
        $cart['sub_total']              = CurrencyManager::formatPrice(CurrencyManager::convertPrice($untaxed));
        $cart['promo']                  = 0; //$untaxed;
        $cart['promo_sub']              = CurrencyManager::formatPrice(CurrencyManager::convertPrice($untaxed));
        $cart['taxes']                  = $taxes;
        $cart['tax_cad']                = $taxes['total_taxes_cad'];
        $cart['tax']                    = CurrencyManager::formatPrice(CurrencyManager::convertPrice($taxes['total_taxes']));
        $cart['total']                  = CurrencyManager::formatPrice(CurrencyManager::convertPrice($total));
        $cart['items']                  = $is;
        $cart['total_unique_items']     = $this->getCount(true);
        $cart['total_items']            = $this->getCount();
        $cart['last_item']              = $lItem;

        $cart['shipping']               = CurrencyManager::formatPrice(CurrencyManager::convertPrice($shipping));
        $cart['free_shipping_remaining_cost_int']   = $isFreeShippping;
        $cart['free_shipping_remaining_cost']       = CurrencyManager::formatPrice($isFreeShippping);
        unset($cart['extras']['shipping']);

        return $cart;
    }

    /**
     * Replaces an old cart with a new cart (case: logging in with a built cart)
     * @param CartInterface $oldCart The old cart (last cart when user was logged)
     * @param CartInterface $newCart The new cart (created while not logged in)
     * @return CartInterface
     */
    public function replace(CartInterface $oldCart, CartInterface $newCart): CartInterface
    {
        $oldCart->open = false;
        $oldCart->expired = true;
        $oldCart->session_id = null;
        
        $newCart->user_id = $oldCart->user_id;
        $newCart->session_id = session()->getId();

        try {
            $oldCart->save();
            $newCart->save();
            event(new CartExpiredEvent($oldCart));
        } catch (\Exception $e) {
            throw new \Exception('error_reseting_cart');
        }

        return $newCart;
    }

    /**
     * Triggers event for given event system configured
     * @param string<\Gratin\Cart\Events\CartEvent> $event The event to launch
     * @param string $action The action executed
     * @param array $extras Any extra data to send to the event
     * @return void
     */
    private function trigger(string $event, string $action, ?array $extras = null): void
    {
        if (!$this->_configs['event_system'] && !isset($this->eventSystem)) {
            // TO DO implement standalone event system + config
        } else {
            $this->eventSystem = $this->_configs['event_system'];
            switch ($this->eventSystem) {
                case "laravel":
                    event(new $event($this->cart, $action, $extras));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Returns the last cart saved (previous to the current one)
     * @return null|\Gratin\Cart\Interfaces\CartInterface
     */
    public function last(): ?CartInterface
    {
        return $this->manager->last($this->cartModelString);
    }

    /**
     * Returns the CartManager
     * @return CartManager
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Builds a cart object from the given bridge
     * @return array
     */
    public function buildCart()
    {
        return $this->bridge::buildCart($this->open());
    }

    public function validProducts($cart)
    {
        return $cart;
    }

    public function getPaymentManager()
    {
        return $this->paymentManagerClass;
    }

    /**
     * Transforms a string into a callable and a callback
     * @param string $string The string to parse
     * @return array<$callable, $callback>
     */
    private function paymentManagerFromString(string $string): array
    {
        list($paymentManagerClass, $paymentManagerMethod) = explode("@", $string);
        $paymentManager       = (new $paymentManagerClass()) ?? null;

        // Validates that the method to be called exists
        if (!method_exists($paymentManager, $paymentManagerMethod)) {
            throw new \Exception('cart.payment_method_not_exists');
        }

        $paymentManagerMethod = $paymentManagerMethod;

        return [$paymentManager, $paymentManagerMethod];
    }
}
