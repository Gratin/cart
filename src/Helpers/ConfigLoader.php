<?php
namespace Gratin\Cart\Helpers;

use Gratin\Cart\Helpers\PhpLoader;
use Gratin\Cart\Helpers\YamlLoader;

class ConfigLoader
{
    /**
     * @var string CONFIG_PHP The string representing the php type loader
     */
    public const CONFIG_PHP         =   "php";

    /**
     * @var string CONFIG_YAML The string representing the yaml type loader
     */
    public const CONFIG_YAML        =   "yaml";

    /**
     * @var string CONFIG_LARAVEL The string representing the laravel type loader
     */
    public const CONFIG_LARAVEL     =   "laravel";

    /**
     * @var string CONFIG_SYMFONY The string representing the symfony type loader
     */
    public const CONFIG_SYMFONY     =   "symfony";

    /**
     * Loads configuration file for multiple sources
     * @param string $file The file path or configuration name to be loaded
     * @param string #type The type of configuration to use (such as built-in configuration
     * functions for Laravel, Symfony, etc.)
     * @return array
     */
    public static function load(string $file, string $type = self::CONFIG_LARAVEL): array
    {
        try {
            switch ($type) {
                case self::CONFIG_LARAVEL:
                    return config($file);
                    break;
                case self::CONFIG_SYMFONY:
                    // return SymfonyConfigLoader::load($file);
                    break;
                case self::CONFIG_PHP:
                    return PhpLoader::load($file);
                    break;
                case self::CONFIG_YAML:
                    return YamlLoader::load($file);
                default:
                    break;
            }
        } catch (\Exception $e) {
            throw new \Exception(__('Unable to load configuration using ' . $type));
        }
    }
}
