<?php
namespace Gratin\Cart\Helpers;

class YamlLoader
{
    public function __construct()
    {
    }

    public function load(string $handle)
    {
        $content = $this->getFileContent($handle);
        $this->rawString = $content;
        try {
            $this->parsedString = yaml_parse($content);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this->parsedString;
    }

    public function get()
    {
    }

    private function getFileContent(string $handle)
    {
        if (!file_exists($handle)) {
            throw new \Exception('Yaml config file does not exists.');
        }

        try {
            $content = file_get_contents($handle);
        } catch (\Exception $e) {
            throw new \Exception('Yaml config cannnot be loaded.');
        }

        return $content;
    }
}
