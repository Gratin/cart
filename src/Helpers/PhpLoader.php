<?php
namespace Gratin\Cart\Helpers;

class PhpLoader
{
    public function __construct()
    {
    }

    public static function load(string $handle)
    {
        $content = self::getFileContent($handle);

        $this->parsedString = $content;

        return $this->parsedString;
    }

    public function get()
    {
    }

    private function getFileContent(string $handle)
    {
        if (!file_exists($handle)) {
            throw new \Exception('Php config file does not exists.');
        }

        try {
            $content = include($handle);
        } catch (\Exception $e) {
            throw new \Exception('Php config cannnot be loaded.');
        }

        return $content;
    }
}
