<?php
namespace Gratin\Cart\Controllers;

use App\Helpers\ProductHelper;
use Illuminate\Http\Request;
use Gratin\Cart\Events\CartEvent;
use Illuminate\Http\JsonResponse;
use Gratin\Cart\Events\CartPaidEvent;
use Illuminate\Routing\Controller;
use \Gratin\Cart\Interfaces\CartInterface;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Redis;

class CartController extends Controller
{
    /**
    * @var string ACTION_ADD Contant for add action string
    */
    public const ACTION_ADD     = "add";

    /**
    * @var string ACTION_REMOVE Contant for remove action string
    */
    public const ACTION_REMOVE  = "remove";

    /**
    * @var string ACTION_DELETE Contant for delete action string
    */
    public const ACTION_DELETE  = "delete";

    /**
    * @var string ACTION_PAY Contant for payment action string
    */
    public const ACTION_PAY     = "pay";

    /**
     * Retrieges the cart
     * @param Illuminate\Http\Request $request The request
     * @return JsonResponse
     */
    public function retrieve(Request $request): JsonResponse
    {
        $cart = \Cart::open();
        $serialized = \Cart::serialize();
        
        if ($request->get('view')) {
            return response()->json(
                [
                    'html'  =>  view()->make(static::VIEW_TEMPLATE, ['data'=>$serialized])->render(),
                    'cart'  =>  $serialized
                ]
            )->setStatusCode(202);
        }

        return response()->json(['cart'=>$serialized])->setStatusCode(202);
    }

    /**
     * Increments or add an item from the cart or sets its quantity
     * @param Illuminate\Http\Request $request The request
     * @return JsonResponse
     */
    public function add(Request $request): JsonResponse
    {
        // require_once '/var/www/xhgui/external/header.php';
        if (!$this->isValidRequest($request, static::ACTION_ADD)) {
            return response()->json(['success'=>false, 'message'=>'request.wrong'])->setStatusCode(422);
        }

        $payload = $this->handleAddRequest($request);
        // $stock = $product->getFlattenStocks();

        if (!$payload) {
            $genericErrors = new MessageBag();
            $genericErrors->add('generic', $genericMessage);

            return response()->json($genericErrors)->setStatusCode(422);
        }

        $cart    = \Cart::add($payload, $request->input('quantity') ?? null);
        $serialized = \Cart::serialize();
        if ($request->get('view')) {
            return response()->json(
                [
                    'html'  =>  (view()->make(static::VIEW_TEMPLATE, ['record'=>\Cart::serialize()])->render()),
                    'cart'  =>  $serialized
                ]
            )->setStatusCode(202);
        }

        return response()->json(['cart'=>$serialized])->setStatusCode(202);
    }

    /**
     * Decrease the quantity of a product from the cart or sets its quantity
     * @param Illuminate\Http\Request $request The request
     * @return JsonResponse
     */
    public function remove(Request $request): JsonResponse
    {
        if (!$this->isValidRequest($request, static::ACTION_REMOVE)) {
            return response()->json(['success'=>false, 'message'=>'request.wrong'])->setStatusCode(422);
        }

        $payload = $this->handleRemoveRequest($request);
        $cart = \Cart::remove($payload);

        $serialized = \Cart::serialize();

        if ($request->get('view')) {
            return response()->json(
                [
                    'html'  =>  (view()->make(static::VIEW_TEMPLATE, ['record'=>$serialized])->render()),
                    'cart'  =>  $serialized
                ]
            )->setStatusCode(202);
        }

        return response()->json(['cart'=>$serialized])->setStatusCode(202);
    }

    /**
     * Process the payment of a cart
     * @param Illuminate\Http\Request $request The request
     * @return JsonResponse
     */
    public function pay(Request $request, $userData = null): JsonResponse
    {
        if (!$this->isValidRequest($request, static::ACTION_PAY)) {
            return response()->json(['success'=>false, 'message'=>'request.wrong'])->setStatusCode(422);
        }

        if (!$userData) {
            $userData = $request;
        }

        \Cart::pay($userData);
        event(new CartPaidEvent($this->get(true)));
        \Cart::forget();

        return response()->json(['success'=>\Cart::serialize()])->setStatusCode(202);
    }

    /**
     * Deletes a product from the cart
     * @param Illuminate\Http\Request $request The request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        if (!$this->isValidRequest($request, static::ACTION_DELETE)) {
            return response()->json(['success'=>false, 'message'=>'request.wrong'])->setStatusCode(422);
        }

        try {
            $payload = $this->handleDeleteRequest($request);
            $cart = \Cart::delete($payload);
        } catch (\Exception $e) {
            return response()->json(['status'=>'error', 'message'=>$e->getMessage()])->setStatusCode(422);
        }

        if ($request->get('view')) {
            return response()->json(
                [
                    'html'  =>  (view()->make(static::VIEW_TEMPLATE, ['record'=>\Cart::serialize()])->render()),
                    'cart'  =>  $cart
                ]
            )->setStatusCode(202);
        }

        return response()->json(['cart'=>\Cart::serialize()])->setStatusCode(202);
    }

    /**
     * Flushes the current cart and creates a new instance
     * @param Illuminate\Http\Request $request The current request
     * @return Illuminate\Http\Response $response
     */
    public function refresh(Request $request): JsonResponse
    {
        \Cart::forget();
        \Cart::open(true);
        return response()->json(\Cart::serialize())->setStatusCode(202);
    }

    /**
     * Triggers the tax process for a given amount and country
     * @param int|float $total The total cost
     * @param string $code The country code
     * @return array
     */
    protected function tax($total, string $code = 'CA'): array
    {
        $taxManager = config('cart.currency_manager');
        $instance   = new $taxManager(config('cart.taxes'));

        $taxables = $this->get(true)->getTaxables(true);
        return $instance->tax($taxables, $total, $code);
    }

    /**
     * Returns the total price for the cart
     * @param Request $request The request
     * @return JsonResponse
     */
    protected function total(Request $request): JsonResponse
    {
        try {
            $cart = \Cart::serialize();
            $total = $cart['total'];
        } catch (\Exception $e) {
            return response()->json(['total'=>0])->setStatusCode(422);
        }

        return response()->json(['total'=>$total])->setStatusCode(202);
    }

    /**
     * Validates a request for a given action
     * @param Illuminate\Http\Request $request The request
     * @param string $action The action
     * @return bool
     */
    protected function isValidRequest(Request $request, string $action): bool
    {
        return true;
    }

    /**
     * Returns the cart
     * @param bool $refresh Wether we should refresh the cart or not
     * @return \Gratin\Cart\Interfaces\CartInterface
     */
    protected function getCart(bool $refresh = false): CartInterface
    {
        return \Cart::open(true);
    }
}
