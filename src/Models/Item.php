<?php
namespace Gratin\Cart\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $table = 'cart_items';

    public $fillable = [
        'product_id',
        'name',
        'unit_price',
        'extras',
        'quantity',
        'taxable',
        'cart_id',
    ];

    public $hidden = [];

    public $casts = [
        'extras'    =>  'array'
    ];
    
    private $product_id;
    private $name;
    private $price;
    private $extras;
    private $quantity;
    private $taxable = false;

    public function isTaxable()
    {
        return $this->_taxable;
    }

    public function price()
    {
        return $this->price;
    }

    public function quantity()
    {
        return $this->quantity;
    }

    public function name()
    {
        return $this->name;
    }

    public function extras()
    {
        return $this->extras;
    }
}
