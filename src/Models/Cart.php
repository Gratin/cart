<?php
namespace Gratin\Cart\Models;

use \DateTimeImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Gratin\Cart\Handlers\CartErrorHandler;
use Gratin\Cart\Interfaces\CartInterface;
use Gratin\Cart\Models\Item;

class Cart extends Model implements CartInterface
{
    public $table = "carts";

    public $fillable = [
        'user_id',
        'session_id',
        'expires_at',
        'expired',
        'open',
        'extras',
    ];

    protected $attributes = [
        'expired'   =>  false,
        'open'      =>  true,
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'expires_at'
    ];

    protected $hidden = ['session_id'];

    protected $casts = [
        'extras' => 'array'
    ];

    public $last_item;
    
    protected $with = ['items'];

    public function checkUser(): bool
    {
        return true;
    }

    public function add(Item $item, ?int $quantity = null)
    {
        $item->cart_id = $this->id;
        $items = $this->items()->where('product_id', $item->product_id)->get();
        
        $existing = null;

        if (!empty($items)) {
            foreach ($items as $i) {
                if ($i->extras['packaging'] === $item->extras['packaging']) {
                    $existing = $i;
                    break;
                }
            }
        }

        \DB::beginTransaction();
        try {
            if ($existing) {
                if ($quantity) {
                    $existing->quantity = $existing->quantity + $quantity;
                } else {
                    $existing->quantity++;
                }
                $existing->save();
            } else {
                $item->save();
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            // throw new \Exception('cant_add_to_cart');
        }

        return $this;
    }

    public function sub(Item $item)
    {
        $item->cart_id = $this->id;
        $existing = $this->items()->where('product_id', $item->product_id)->where('extras', 'like', '%'.$item->extras['packaging'].'%')->first();
        
        \DB::beginTransaction();
        try {
            if ($existing) {
                $existing->quantity--;
                if ($existing->quantity <= 0) {
                    $existing->forceDelete();
                } else {
                    $existing->save();
                }
            } else {
                $item->save();
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw new $e;
        }

        return $this;
    }

    public function remove(Item $item)
    {
        $existing = $this->items()->where('product_id', $item->product_id)->where('extras', 'like', '%'.$item->extras['packaging'].'%')->first();

        if ($existing) {
            \DB::beginTransaction();
            try {
                $existing->forceDelete();
                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollback();
                throw new \Exception('cant_removed_from_cart');
            }
        }

        return $this;
    }

    public function setLastItem(Item $item)
    {
        $this->last_item = $item;
    }

    public function items()
    {
        return $this->hasMany('\Gratin\Cart\Models\Item', 'cart_id', 'id');
    }

    /**
     * Get cart by odoo order id
     *
     * @param Builder $query
     * @param int $orderId
     * @return Builder
     */
    public function scopeByOdooOrderId(Builder $query, int $orderId):Builder
    {
        return $query->where('extras', 'LIKE', '%"odoo_order_id":'.$orderId.'%');
    }

    /**
     * Find by active session ID and NOT expired
     * @param Builder $query
     * @return $this
     */
    public function scopeBySessionId(Builder $query, string $sessionId)
    {
        return $query->where('session_id', $sessionId)
                     ->where('expired', 0)
                     ->whereNull('user_id');
    }
}
