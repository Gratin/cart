<?php
namespace Gratin\Cart\Handlers;

use Exception;

class CartErrorHandler extends Exception
{
    public const DELETE_STRING 		= "errors.delete";
    public const TRANSFORMER_ERROR  = "errors.transformer";
    public const PAYMENT_FAILED 	= "errors.payment.failed";
    
    public static function delete($product)
    {
        throw new self(__(self::DELETE_STRING, ['product'=>$product->name]));
    }

    public static function transformer($className)
    {
        throw new self(__(self::TRANSFORMER_ERROR, ['class'=>$className]));
    }

    public static function paymentFailed()
    {
        throw new self(__(self::PAYMENT_FAILED));
    }
}
