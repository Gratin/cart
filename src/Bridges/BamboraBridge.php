<?php
namespace Gratin\Cart\Bridges;

class BamboraBridge
{
    /**
     * @static $template A template array of requirements for payment request
     */
    protected static $template = ['amount' => 0, 'payment_method' => ''];
}
