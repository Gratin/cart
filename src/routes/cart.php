<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/cart/refresh', config('cart.controller').'@refresh')->name('cart.refresh');
    Route::get('/cart', config('cart.controller').'@retrieve')->name('cart');
    Route::get('/cart/shipping', config('cart.controller').'@shipping')->name('cart.shipping');

    Route::post('/cart/add', config('cart.controller').'@add')->name('cart.add');
    Route::post('/cart/remove', config('cart.controller').'@remove')->name('cart.remove');
    Route::post('/cart/delete', config('cart.controller').'@delete')->name('cart.delete');
    Route::post('/cart/process', config('cart.controller').'@pay')->name('cart.process');
});
