<?php
namespace Gratin\Cart\Facades;

use Illuminate\Support\Facades\Facade;

use Gratin\Cart\Services\CartService;

class CartFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CartService::class;
    }
}
